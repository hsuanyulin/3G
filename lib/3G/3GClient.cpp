#include "3GClient.h"
#include "ATCommands.h"
// initilizer list
// "ownership" coupled to class lifetime






GGGClient::GGGClient(const int RXPin, const int TXPin): _RXPin(RXPin), _TXPin(TXPin), SerialAT(new SoftwareSerial(RXPin,TXPin)){
  return;
}

GGGClient::~GGGClient(){
  if(SerialAT != nullptr) delete SerialAT;
  return;
}

bool GGGClient::begin(){
  moduleSetup();
  initiateRegistration();
  bearerSetup();
  internetSetup();
}

String* GGGClient::getKeyToParse(){
  return KeyWordValue;
}

void GGGClient::setKeyToParse(String givenKey[]){
  KeyWord = givenKey;
  keyWordEnable = true;
  return;
}


void GGGClient::handleResponse(){
  response = "";
  int i = 0;
	//keep sensing if data is arriving
    while (i < TIMEOUT){
      while (SerialAT->available() > 0) {	//wait for data at software serial
        char c = SerialAT->read();
        response = response + c;
        Serial.write(c);	//Send data recived from software serial to hardware serial
      }


      if (i > OPERATIONTIME_MIN){
          if(      response.indexOf("OK") >= 0
                || response.indexOf("ERROR") >= 0){
          // NOTE: not sure if correct break idendifiers
          break;
          }
      }

      delay(51);
      i ++;
    }

		if(timeEnable == true){

      if (response.indexOf("+CIPGSMLOC:") > -1){
        delay(100);

        while (SerialAT->available() > 0) {	//wait for data at software serial
          char c = SerialAT->read();
          response = response + c;
          Serial.write(c);	//Send data recived from software serial to hardware serial
        }

        int beginIndexOfLocation = response.indexOf("+CIPGSMLOC:")+ 14;
        location = response.substring(beginIndexOfLocation+10,beginIndexOfLocation+19) +","+ response.substring(beginIndexOfLocation,beginIndexOfLocation+9);
      }

      while (SerialAT->available() > 0) {	//wait for data at software serial
        char c = SerialAT->read();
        response = response + c;
        Serial.write(c);	//Send data recived from software serial to hardware serial
      }

      if (location.length()>1){

        delay(100);
        while (SerialAT->available() > 0) {	//wait for data at software serial
          char c = SerialAT->read();
          response = response + c;
          Serial.write(c);	//Send data recived from software serial to hardware serial
        }

        int beginIndexOfTime = response.indexOf(',')+1;
        time = response.substring(beginIndexOfTime);
        beginIndexOfTime = time.indexOf(',')+1;
        time = time.substring(beginIndexOfTime);
        beginIndexOfTime = time.indexOf(',')+1;
        time = time.substring(beginIndexOfTime);
        int endIndexOfTime = time.indexOf(',');
        time = time.substring(0,endIndexOfTime);

      }

      if(time.length()>1 && location.length()>1){
        timeEnable = false;
      }
    }

		if( keyWordEnable == true){
			while (SerialAT->available() > 0) {	//wait for data at software serial
				char c = SerialAT->read();
				response = response + c;
				Serial.write(c);	//Send data recived from software serial to hardware serial
			}

			int keyWordLength = 0;

      int j = 0;
			while( j < KEYWORD_MAX && keyWordLength == 0 ) {
        //check how many key words are needed to be parsed

				int length = 0;
				length = KeyWord[j].length();

				if (length == 0) {
					keyWordLength = j;
					break;
				}
				delay(50);
				j++;
			}


			//parse all the keyword values
			j = 0;
			while( j < keyWordLength ) {

				if(response.indexOf(KeyWord[j]) > 0) {

					while (SerialAT->available() > 0) {	//wait for data at software serial
		        char c = SerialAT->read();
		        response = response + c;
		        Serial.write(c);	//Send data recived from software serial to hardware serial
		      }

					int keyWordBegin = response.indexOf(KeyWord[j]) + KeyWord[j].length();
	        KeyWordValue[j] = response.substring(keyWordBegin);
          const int COLON_SIZE = 1;
					keyWordBegin = KeyWordValue[j].indexOf(':') + COLON_SIZE;
					KeyWordValue[j] = KeyWordValue[j].substring(keyWordBegin);

          const int BEGIN_QOTATION_INDEX = 2;
          const int QOTATION_SIZE = 1;
          KeyWordValue[j] = KeyWordValue[j].substring(BEGIN_QOTATION_INDEX);
	        int keyWordEnd =  KeyWordValue[j].indexOf('\"') + QOTATION_SIZE;
	        KeyWordValue[j] = KeyWordValue[j].substring(0,keyWordEnd);
					//check if int or string bcecause int has no ""
					int length = 0;
					if( KeyWordValue[j].indexOf(',') == KeyWordValue[j].length()-2){
						KeyWordValue[j] = KeyWordValue[j].substring(0,KeyWordValue[j].indexOf(','));
					}
				  String str = KeyWordValue[j].substring(0,1);
					if(str == " "){
						KeyWordValue[j] = KeyWordValue[j].substring(1);
					}
				}
				j++;
			}

			//Check if all key words are parsed
			bool KeyWordComplete = true;
			j = 0; //reset counter

			while( j < keyWordLength ){
				KeyWordComplete = KeyWordValue[j].length()>1 && KeyWordComplete;
				j++;
			}

      if(KeyWordComplete){
        keyWordEnable = false;
      }

    }
}

void GGGClient::moduleSetup(){
  SerialAT->begin(BAUD_RATE);

  Serial.println(F("\n[ LOG ] Enable error code... "));
  SerialAT->println(F(CMD_ERROR_ACTIVE));
  handleResponse();
  Serial.println(F("\n[ LOG ] Checking registration status..."));
  SerialAT->println(F(CMD_REGISTER_CHECK));
  handleResponse();
}

void GGGClient::initiateRegistration(){
  Serial.println(F("\n[ LOG ] UNRegistering..."));
  SerialAT->println(F(CMD_REGISTER_UNDO));
  handleResponse();
  Serial.println(F("\n[ LOG ] Registering with location information..."));
  SerialAT->println(F(CMD_REGISTER_ACTIVE));
  handleResponse();
  Serial.println(F("\n[ LOG ] Checking registration status..."));
  SerialAT->println(F(CMD_REGISTER_CHECK));
  handleResponse();
}

void GGGClient::bearerSetup(){
  Serial.println(F("\n[ LOG ] Configure bearer profile 1..."));
  SerialAT->println(F(CMD_BEARER_SET_CONTENT));
  handleResponse();
  Serial.println(F("\n[ LOG ] Configure bearer profile 1..."));
  SerialAT->println(F(CMD_BEARER_SET_APN));
  handleResponse();
  Serial.println(F("\n[ LOG ] Set up bearer 1 to open.... "));
  SerialAT->println(F(CMD_BEARER_ACTIVE));
  handleResponse();
  Serial.println(F("\n[ LOG ] Check whether bearer 1 is open.... "));
  SerialAT->println(F(CMD_BEARER_CHECK));
  handleResponse();

}

void GGGClient::internetSetup(){
  Serial.println(F("\n[ LOG ] Activate PDP... "));
  SerialAT->println(F(CMD_PDP_SET_APN));
  handleResponse();
  Serial.println(F("\n[ LOG ] Check the PDP context..."));
  SerialAT->println(F(CMD_PDP_CHECK));
  handleResponse();

  Serial.println(F("\n[ LOG ] APN setting up "));
  SerialAT->println(F(CMD_CARRIER_SET_APN));
  handleResponse();
  Serial.println(F("\n[ LOG ] APN setting up check..."));
  SerialAT->println(F(CMD_CARRIER_CHECK));
  handleResponse();
  Serial.println(F("\n[ LOG ] Start wireless connection with the GPRS... "));
  SerialAT->println(F(CMD_CARRIER_ACTIVE));
  handleResponse();
  Serial.println(F("\n[ LOG ] Gets the IP address... "));
  SerialAT->println(F(CMD_CARRIER_IP));
  handleResponse();
  Serial.println(F("\n[ LOG ] Gets the signal strength... "));
  SerialAT->println(F(CMD_CARRIER_SIGNAL));
  handleResponse();
  Serial.println(F("\n[ LOG ] Attach from GPRS Service... "));
  SerialAT->println(F(CMD_GPRS_ACTIVE));
  handleResponse();
  Serial.println(F("[ LOG ] Check if it is attached or detached from GPRS Service... "));
  SerialAT->println(F(CMD_GPRS_CHECK));
  handleResponse();
}

int GGGClient::postHTTP(String url, String paylaod){
  //it takes more time for HTTP-related commands to execute
  //so it delays 500ms here

  String str = CMD_HTTP_BEARER_URL "\"" + url + "\"";
	int postLength = paylaod.length();
	String cmd = CMD_HTTP_POST_BODY + String(postLength) + CMD_HTTP_POST_LATENCY;

  delay(500);
  Serial.println(F("\n[ LOG ] Initiate HTTP service... "));
  SerialAT->println(F(CMD_HTTP_ACTIVE));
  handleResponse();
  delay(500);
  Serial.println(F("\n[ LOG ] Set the HTTP bearer number... "));
  SerialAT->println(F(CMD_HTTP_BEARER_NUMBER));
  handleResponse();
  delay(500);
  Serial.println(F("\n[ LOG ] Set the HTTP CONTENT type... "));
  SerialAT->println(F(CMD_HTTP_BEARER_CONTENT));
  handleResponse();
  delay(500);
  Serial.println(F("\n[ LOG ] Set the HTTP URL... "));
  SerialAT->println(str);
  handleResponse();
  delay(100);
	Serial.println(F("\n[ LOG ] Start the HTTP session... "));
	SerialAT->print(cmd);
  //not using sendATCommands() here is because
  //after cmd is sent, the body of HTTP REQ should be sent immediatly
  //not reading the response from the serial port
  delay(100);
	Serial.println(F("\n[ LOG ] Set the content of the HTTP body... "));
	SerialAT->println(paylaod);
  handleResponse();
	Serial.println(F("\n[ LOG ] Start the HTTP session... "));
	SerialAT->println(F(CMD_HTTP_POST_SEND));
  handleResponse();
	Serial.println(F("\n[ LOG ] To read the data of the HTTP server... "));
	SerialAT->println(F(CMD_HTTP_RESPONSE));
  handleResponse();
  Serial.println(F("\n[ LOG ] Terminate HTTP server... "));
  SerialAT->println(F(CMD_HTTP_TERMINATE));
  handleResponse();
}

int GGGClient::getHTTP(String URL, String path){

  String str = CMD_TCP_URL "\"" + URL + "\"" CMD_TCP_PORT;

  Serial.println(F("\n[ LOG ] Select Data Transmitting Mode... "));
  SerialAT->println(F(CMD_TCP_DATA_MODE));
  handleResponse();
  Serial.println(F("\n[ LOG ] Set up TCP connection... "));
  SerialAT->println(str);
  handleResponse();

  if (response.indexOf("CONNECT") > 0 && response.indexOf("TCP") > 0){

    str = "GET " + path +" HTTP/1.1\r\n";
    str = str + "Host: " + URL + "\r\n";
    str = str + "Content-type: application/json\r\n";
    str = str + "Cache-Control: no-cache\r\n";
    str = str + "\r\n\r\n";
    int post_length = str.length();
    String cmd = CMD_TCP_SEND + String(post_length);

    Serial.println(F("\n[ LOG ] Send data through TCP connection... "));
    SerialAT->println(cmd);
    handleResponse();
    delay(100);
    SerialAT->println(str);
    handleResponse();
  }
    Serial.println(F("\n[ LOG ] Check IP connection status... "));
    SerialAT->println(F(CMD_TCP_CHECK));
    handleResponse();
		delay(100);
    Serial.println(F("\n[ LOG ] Close IP connection... "));
    SerialAT->println(F(CMD_TCP_TERMINATE));
    handleResponse();
}


String GGGClient::coordinates(){
	int TimeOut = 0;
  while( TimeOut < 10 && location.length()<1 ){ //10 times re-try
    if(locationCoordinate()){
			break;
    }
    int ResponseTimeOut = 3;
		int h = 0;
    while (SerialAT->available() == 0 && h < ResponseTimeOut && time.length() < 1 ) {	//wait for data at software serial
      handleResponse();
      h++;
    }
    TimeOut++;
  }
	return location;
}


bool GGGClient::locationCoordinate(){
  if(location.length()<1){
    timeEnable = true;
    Serial.println(F("[ LOG ] Test location and the time... "));
    SerialAT->println(F(CMD_HTTP_BEARER_NUMBER));
    handleResponse();

		// if the time is retrieved, the location is surely available as well.
		// However it is not true the other way around.

    if (time.length()>1){
      delay(400);
      Serial.println(F("\n[ LOG ] Current time is ..."));
      Serial.println(time);
      Serial.println(F("\n[ LOG ] Current location is ..."));
      Serial.println(location);
      return true;
    } else{
      handleResponse();
      if(time.length()>1){
        delay(400);
        Serial.println(F("\n[ LOG ] Current time is ..."));
        Serial.println(time);
        Serial.println(F("\n[ LOG ] Current location is ..."));
        Serial.println(location);
        return true;
      } else {
        return false;
      }
    }
  } else{
    delay(500);
    return true;
  }
}



void GGGClient::write(char c){
  SerialAT->write(c);
}

char GGGClient::read(){
  if (SerialAT->available() > 0)
  {
    return SerialAT->read();
  } else{
    Serial.println(F("\n[ WARNING ] No value from the soft serial port to read..."));
  }

}
