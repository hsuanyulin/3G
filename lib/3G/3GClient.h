
#include <Arduino.h>
#include <memory>
#include <SoftwareSerial.h>


class SoftwareSerial;

class GGGClient{
private:
  const int BAUD_RATE = 9600;
  const int _RXPin;
  const int _TXPin;
  const int KEYWORD_MAX = 5;
  const int TIMEOUT = 201;
  const int OPERATIONTIME_MIN = 21;

  String time;
  String* KeyWord = new String[KEYWORD_MAX];
  String* KeyWordValue = new String[KEYWORD_MAX];
  bool keyWordEnable = false;

  SoftwareSerial *SerialAT;
  //std::unique_ptr<SoftwareSerial> serialInstance;

  bool timeEnable = false;
  String response; // read = read char by char
  String location;

  void handleResponse();
  void moduleSetup();
  void initiateRegistration();
  void bearerSetup();
  void internetSetup();
  bool locationCoordinate();
  void sendATCommands(const String cmd);

  public:
  GGGClient(const int RXPin = 14, const int TXPin = 2);
  ~GGGClient();

  bool begin();
  void write(char c);
  char read();

// not hurting
  int postHTTP(const String url, const String paylaod);
  int getHTTP(const String url, const String path);

  String coordinates();

  String* getKeyToParse();
  void setKeyToParse(String givenKey[]);


};
