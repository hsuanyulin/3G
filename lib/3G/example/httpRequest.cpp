#include <Arduino.h>
#include <SoftwareSerial.h>
#include <3GClient.h>

GGGClient GGGClient;

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ;
  }
  delay(8000);
  GGGClient.begin();
  //Get lcoation
  String location = "";
  location = GGGClient.coordinates();

  delay(300);
  //get address
  GGGClient.keyWord_enable = 1;
  GGGClient.keyWord[0] = "\"formatted_address\"";
  String url = "maps.googleapis.com";
  String path = "/maps/api/geocode/json?latlng="+ location +"&amp;result_type=administrative_area_level_1&amp;key=AIzaSyDnMUmBQYDygc-6-L74HE2dVkofI_JT-IA";
  GGGClient.getHTTP(url, path);
  GGGClient.keyWord_enable = 0;

  int m = 0;
  while ( m < 99999){
    //make sure address is retrieved
    if (GGGClient.KeyWord_value[0].length()>1 && GGGClient.KeyWord_value[1].length()<1 ){
      delay(100);
      GGGClient.keyWord_enable = 1;
      GGGClient.keyWord[1] = "\"temp\"";
      GGGClient.keyWord[2] = "\"description\"";
      delay(200);
      String web_URL = "api.openweathermap.org";
      String lon = location.substring(10,location.length());
      String path = "/data/2.5/weather?lat="+ String(location.toFloat()) + "&lon=" + String(lon.toFloat())+"&appid=96b8c7454acfb29cbbc20835282972ee";
      GGGClient.getHTTP(web_URL, path);
      // Wait unitl info is completly received and disable it
      int p =0;
      while ( p < 9999 ){
        if (GGGClient.KeyWord_value[2].length()>1 && GGGClient.keyWord_enable == 0){
          delay(100);
          Serial.println("\n[ LOG ] Address of the city is ...");
          Serial.println(GGGClient.KeyWord_value[0]);
          Serial.println("\n[ LOG ] Temperature of the city is ...");
          Serial.println(GGGClient.KeyWord_value[1]);
          Serial.println("\n[ LOG ] Weather of the city is ...");
          Serial.println(GGGClient.KeyWord_value[2]);
          GGGClient.keyWord_enable == 0;
          break;
        }
        p++;
        delay(200);
      }
    }
    if(GGGClient.KeyWord_value[2].length()>1 && GGGClient.KeyWord_value[1].length()>1){
      break;
    }
    delay(50);
  }
  //post it on Rysta server
  String paylaod = "{\"Time\":\""+ GGGClient.time + "\"," + "\"City\":" + GGGClient.KeyWord_value[0] + ",";
  paylaod = paylaod + "\"Coordinates\":\"" + location + "\"," + "\"Temperature\":" + GGGClient.KeyWord_value[1] + ",";
  paylaod = paylaod + "\"Weather\":" + GGGClient.KeyWord_value[2] + "}";
  Serial.println(paylaod);
  String filename = "time" + GGGClient.time;
  url = "device.rysta-api.com/v0/devices/KT34P22/devicefile/" + filename;
  GGGClient.postHTTP(url, paylaod);

  Serial.println("\n[ LOG ] Loop started...");
}
