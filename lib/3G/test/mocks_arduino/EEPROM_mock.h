#ifndef EEPROM_h
#define EEPROM_h

#include <stddef.h>
#include <stdint.h>
#include <string.h>

class EEPROMClass {
public:
  EEPROMClass(uint32_t sector);
  EEPROMClass(void);

  void begin(size_t size);
  uint8_t read(int address);
  void write(int address, uint8_t val);
  bool commit();
  void end();

  uint8_t * getDataPtr();

  template<typename T>
  T &get(int address, T &t) {
    if (address < 0 || address + sizeof(T) > _size)
      return t;

    memcpy((uint8_t*) &t, _data + address, sizeof(T));
    return t;
  }

  template<typename T>
  const T &put(int address, const T &t) {
    if (address < 0 || address + sizeof(T) > _size)
      return t;

    memcpy(_data + address, (const uint8_t*) &t, sizeof(T));
    _dirty = true;
    return t;
  }

protected:
  uint32_t _sector;
  uint8_t* _data;
  size_t _size;
  bool _dirty;

};


#endif
