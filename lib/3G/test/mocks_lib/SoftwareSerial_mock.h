#ifndef SoftwareSerial_h
#define SoftwareSerial_h

#include <inttypes.h>

class SoftwareSerial{

public:
   SoftwareSerial(int receivePin, int transmitPin);
   ~SoftwareSerial();

   void begin(long speed);
   void setTransmitEnablePin(int transmitEnablePin);

   int peek();

   int write(uint8_t byte){ return true;};
   int read(){ return true;};
   int available(){ return true;};
   void flush(){;};

   void enableRx(bool on){};
   void rxRead(){};

};

#endif
