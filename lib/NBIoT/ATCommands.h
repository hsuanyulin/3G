
#define CMD_MODULE_MODELID        "AT+CGMM"
#define CMD_MODULE_MANUFSCTURERID "AT+CGMI"
#define CMD_MODULE_FIRMWARE       "AT+CGMR"
#define CMD_MODULE_IMEI           "AT+CGSN"
#define CMD_MODULE_ATCOMMANDS     "AT+CLAC"
#define CMD_MODULE_FULLFUNCTION   "AT+CFUN=1"
#define CMD_MODULE_ERROR          "AT+CMEE=2"
#define CMD_MODULE_GENERALINFO    "AT+NUESTATS"
#define CMD_MODULE_AUTOCONNECT    "AT+NCONFIG=AUTOCONNECT,TRUE"

#define CMD_NETWORK_SIGNAL           "AT+CSQ"
#define CMD_NETWORK_OPERATOR_CHECK   "AT+COPS?"
#define CMD_NETWORK_OPERATOR_AUTO    "AT+COPS=0,0"
#define CMD_NETWORK_CONNECTION_CHECK "AT+CSCON?"
#define CMD_NETWORK_CONNECTION_AUTO  "AT+CSCON=1"
#define CMD_NETWORK_BAND_CHECK       "AT+NBAND?"

#define CMD_PDP_SET_APN           "AT+CGDCONT=1,\"IP\",\"web.vodafone.de\""
#define CMD_PDP_CHECK             "AT+CGDCONT?"
#define CMD_PDP_ADDRESS           "AT+CGPADDR=1"

#define CMD_GPRS_ACTIVE           "AT+CGATT=1"
#define CMD_GPRS_CHECK            "AT+CGATT?"

#define CMD_REGISTER_UNDO         "AT+CEREG=0"
#define CMD_REGISTER_ACTIVE       "AT+CEREG=2"
#define CMD_REGISTER_CHECK        "AT+CEREG?"

#define CMD_UDP_SOCKET            "AT+NSOCR=DGRAM,17,42000,1"
#define CMD_UDP_WRITE             "AT+NSOST=1,"
#define CMD_UDP_READ              "AT+NSORF=1,20" //length = 20
#define CMD_UDP_CLOSE             "AT+NSOCL=1"
#define CMD_PING                  "AT+NPING=137.226.107.63"

#define CMD_COAP_IP_CONFIG    "AT+NCDP="
#define CMD_COAP_READ         "AT+NMGR"
#define CMD_COAP_MSGINDICATE  "AT+NNMI=1"
#define CMD_COAP_WRITE        "AT+NMGS="
#define CMD_COAP_UL_CHECK     "AT+NQMGS"
