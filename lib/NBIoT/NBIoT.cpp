#include "NBIoT.h"
#include "ATCommands.h"
// initilizer list
// "ownership" coupled to class lifetime






NBIoT::NBIoT(const int RXPin, const int TXPin): _RXPin(RXPin), _TXPin(TXPin), SerialAT(new SoftwareSerial(RXPin,TXPin)){
  return;
}

NBIoT::~NBIoT(){
  if(SerialAT != nullptr) delete SerialAT;
  return;
}

bool NBIoT::begin(){
  moduleSetup();
  initiateRegistration();
  networkSetup();
  internetSetup();
}

void NBIoT::UDPConnect(String IP, String port, char *data){
  initiateRegistration();
  networkSetup();
  internetSetup();
  UDPSocket(IP, port, data);
}

void NBIoT::COAPConnect(String IP, String port, char *data){
  initiateRegistration();
  networkSetup();
  internetSetup();
  COAPDatagram(IP, port, data);
}

String* NBIoT::getKeyToParse(){
  return KeyWordValue;
}

void NBIoT::setKeyToParse(String givenKey[]){
  KeyWord = givenKey;
  keyWordEnable = true;
  return;
}


void NBIoT::handleResponse(){
  response = "";
  int i = 0;
	//keep sensing if data is arriving
    while (i < TIMEOUT){
      while (SerialAT->available() > 0) {	//wait for data at software serial
        char c = SerialAT->read();
        response = response + c;
        Serial.write(c);	//Send data recived from software serial to hardware serial
      }


      if (i > OPERATIONTIME_MIN){
          if( response.indexOf("OK") >= 0
              || response.indexOf("ERROR") >= 0){
          // NOTE: not sure if correct break idendifiers
          break;
          }
      }
      delay(51);
      i ++;
    }

		if( keyWordEnable == true){
			while (SerialAT->available() > 0) {	//wait for data at software serial
				char c = SerialAT->read();
				response = response + c;
				Serial.write(c);	//Send data recived from software serial to hardware serial
			}

			int keyWordLength = 0;
      int j = 0;
			while( j < KEYWORD_MAX && keyWordLength == 0 ) {
        //check how many key words are needed to be parsed

				int length = 0;
				length = KeyWord[j].length();

				if (length == 0) {
					keyWordLength = j;
					break;
				}
				delay(50);
				j++;
			}
			//parse all the keyword values
			j = 0;
			while( j < keyWordLength ) {

				if(response.indexOf(KeyWord[j]) > 0) {

					while (SerialAT->available() > 0) {	//wait for data at software serial
		        char c = SerialAT->read();
		        response = response + c;
		        Serial.write(c);	//Send data recived from software serial to hardware serial
		      }

					int keyWordBegin = response.indexOf(KeyWord[j]) + KeyWord[j].length();
	        KeyWordValue[j] = response.substring(keyWordBegin);
          const int COLON_SIZE = 1;
					keyWordBegin = KeyWordValue[j].indexOf(':') + COLON_SIZE;
					KeyWordValue[j] = KeyWordValue[j].substring(keyWordBegin);

          const int BEGIN_QOTATION_INDEX = 2;
          const int QOTATION_SIZE = 1;
          KeyWordValue[j] = KeyWordValue[j].substring(BEGIN_QOTATION_INDEX);
	        int keyWordEnd =  KeyWordValue[j].indexOf('\"') + QOTATION_SIZE;
	        KeyWordValue[j] = KeyWordValue[j].substring(0,keyWordEnd);
					//check if int or string bcecause int has no ""
					int length = 0;
					if( KeyWordValue[j].indexOf(',') == KeyWordValue[j].length()-2){
						KeyWordValue[j] = KeyWordValue[j].substring(0,KeyWordValue[j].indexOf(','));
					}
				  String str = KeyWordValue[j].substring(0,1);
					if(str == " "){
						KeyWordValue[j] = KeyWordValue[j].substring(1);
					}
				}
				j++;
			}

			//Check if all key words are parsed
			bool KeyWordComplete = true;
			j = 0; //reset counter

			while( j < keyWordLength ){
				KeyWordComplete = KeyWordValue[j].length()>1 && KeyWordComplete;
				j++;
			}

      if(KeyWordComplete){
        keyWordEnable = false;
      }
    }
}

void NBIoT::moduleSetup(){
  SerialAT->begin(BAUD_RATE);

  Serial.println("\n[ LOG ] Model identification... ");
  SerialAT->println(F(CMD_MODULE_MODELID));
  handleResponse();
  Serial.println("\n[ LOG ] Manufacturer identification... ");
  SerialAT->println(F(CMD_MODULE_MANUFSCTURERID));
  handleResponse();

  Serial.println("\n[ LOG ] Firmware version identification... ");
  SerialAT->println(F(CMD_MODULE_FIRMWARE));
  handleResponse();
  Serial.println("\n[ LOG ] IMEI identification... ");
  SerialAT->println(F(CMD_MODULE_IMEI));
  handleResponse();
  Serial.println("\n[ LOG ] List all available AT commands... ");
  SerialAT->println(F(CMD_MODULE_ATCOMMANDS));
  handleResponse();

  Serial.println(F("\n[ LOG ] Enable error code... "));
  SerialAT->println(F(CMD_MODULE_ERROR));
  handleResponse();
  Serial.println(F("\n[ LOG ] Enable full functions..."));
  SerialAT->println(F(CMD_MODULE_FULLFUNCTION));
  handleResponse();
  Serial.println(F("\n[ LOG ] Checking general info..."));
  SerialAT->println(F(CMD_MODULE_GENERALINFO));
  handleResponse();
  Serial.println(F("\n[ LOG ] Set auto connection..."));
  SerialAT->println(F(CMD_MODULE_AUTOCONNECT));
  handleResponse();
}

void NBIoT::initiateRegistration(){
  Serial.println(F("\n[ LOG ] UNRegistering..."));
  SerialAT->println(F(CMD_REGISTER_UNDO));
  handleResponse();
  Serial.println(F("\n[ LOG ] Registering with location information..."));
  SerialAT->println(F(CMD_REGISTER_ACTIVE));
  handleResponse();
  Serial.println(F("\n[ LOG ] Checking registration status..."));
  SerialAT->println(F(CMD_REGISTER_CHECK));
  handleResponse();
}

void NBIoT::networkSetup(){
  Serial.println(F("\n[ LOG ] Check signal strength..."));
  SerialAT->println(F(CMD_NETWORK_SIGNAL));
  handleResponse();
  Serial.println(F("\n[ LOG ] Check operator..."));
  SerialAT->println(F(CMD_NETWORK_OPERATOR_CHECK));
  handleResponse();
  Serial.println(F("\n[ LOG ] Set searching for operator automatically..."));
  SerialAT->println(F(CMD_NETWORK_OPERATOR_AUTO));
  handleResponse();
  Serial.println(F("\n[ LOG ] Set connection status signalling..."));
  SerialAT->println(F(CMD_NETWORK_CONNECTION_AUTO));
  handleResponse();
  Serial.println(F("\n[ LOG ] Check connection status..."));
  SerialAT->println(F(CMD_NETWORK_CONNECTION_CHECK));
  handleResponse();
}

void NBIoT::internetSetup(){
  Serial.println(F("\n[ LOG ] Activate PDP... "));
  SerialAT->println(F(CMD_PDP_SET_APN));
  handleResponse();
  Serial.println(F("\n[ LOG ] Check the PDP context..."));
  SerialAT->println(F(CMD_PDP_CHECK));
  handleResponse();

  Serial.println(F("\n[ LOG ] Attach from GPRS Service... "));
  SerialAT->println(F(CMD_GPRS_ACTIVE));
  handleResponse();
  Serial.println(F("[ LOG ] Check if it is attached or detached from GPRS Service... "));
  SerialAT->println(F(CMD_GPRS_CHECK));
  handleResponse();

  Serial.println(F("\n[ LOG ] Check the PDP address..."));
  SerialAT->println(F(CMD_PDP_ADDRESS));
  handleResponse();

  Serial.println(F("\n[ LOG ] Test if connected to the internet by ping..."));
  SerialAT->println(F(CMD_PING));
  handleResponse();
}

void NBIoT::UDPSocket(String IP, String port, char *data){
    Serial.println(F("\n[ LOG ] Test if connected to the internet by ping..."));
    SerialAT->println(F(CMD_PING));
    handleResponse();

    Serial.println(F("\n[ LOG ] Open a UDP socket..."));
    SerialAT->println(F(CMD_UDP_SOCKET));
    handleResponse();

    String dataHEX = hex2str(data);
    String dataLength = String(sizeof(data));
    String str = CMD_UDP_WRITE + IP + "," + port + "," + dataLength + "," + dataHEX;
    Serial.println(F("\n[ LOG ] Send UDP datagram..."));
    SerialAT->println(str);
    handleResponse();
    Serial.println(F("\n[ LOG ] Read UDP response with length 20..."));
    SerialAT->println(F(CMD_UDP_READ));
    handleResponse();
    Serial.println(F("\n[ LOG ] Close UDP socket..."));
    SerialAT->println(F(CMD_UDP_CLOSE));
    handleResponse();
}

void NBIoT::COAPDatagram(String IP, String port, char *data){
    Serial.println(F("\n[ LOG ] Test if connected to the internet by ping..."));
    SerialAT->println(F(CMD_PING));
    handleResponse();

    String str = CMD_COAP_IP_CONFIG + IP + "," + port; //IP and port is temperary
    Serial.println(F("\n[ LOG ] Config host IP address..."));
    SerialAT->println(str);
    handleResponse();

    Serial.println(F("\n[ LOG ] Enable message indicator..."));
    SerialAT->println(F(CMD_COAP_MSGINDICATE));
    handleResponse();

    //hexadecimal to text
    //4772656574696e672066726f6d205279737461 to "Greeting from Rysta"
    String dataHEX = hex2str(data);
    String dataLength = String(sizeof(data));
    str = CMD_COAP_WRITE + dataLength + "," + dataHEX;
    Serial.println(F("\n[ LOG ] Send COAP datagram..."));
    SerialAT->println(str);
    handleResponse();
    Serial.println(F("\n[ LOG ] Check COAP datagram uplink status..."));
    SerialAT->println(F(CMD_COAP_UL_CHECK));
    handleResponse();
    Serial.println(F("\n[ LOG ] Read COAP datagram response..."));
    SerialAT->println(F(CMD_COAP_READ));
    handleResponse();

}

void NBIoT::write(char c){
  SerialAT->write(c);
}

char NBIoT::read(){
  if (SerialAT->available() > 0)
  {
    return SerialAT->read();
  } else{
    Serial.println(F("\n[ WARNING ] No value from the soft serial port to read..."));
  }
}

char NBIoT::nibble2c(char c)
{
   if ((c>='0') && (c<='9'))
      return c-'0';
   if ((c>='A') && (c<='F'))
      return c+10-'A';
   if ((c>='a') && (c<='a'))
      return c+10-'a';
   return -1 ;
}

char NBIoT::hex2c(char c1, char c2)
{
   if(nibble2c(c2) >= 0)
     return nibble2c(c1)*16+nibble2c(c2) ;
   return nibble2c(c1) ;
}

String NBIoT::hex2str(char *data)
{
   String result = "" ;
   for (int i=0 ; nibble2c(data[i])>=0 ; i++)
   {
      result += hex2c(data[i],data[i+1]) ;
      if(nibble2c(data[i+1])>=0)
        i++ ;
   }
   return result;
}
