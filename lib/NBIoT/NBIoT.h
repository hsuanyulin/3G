
#include <Arduino.h>
#include <memory>
#include <SoftwareSerial.h>


class SoftwareSerial;

class NBIoT{
private:
  const int BAUD_RATE = 9600;
  const int _RXPin;
  const int _TXPin;
  const int KEYWORD_MAX = 5;
  const int TIMEOUT = 201;
  const int OPERATIONTIME_MIN = 21;

  String time;
  String* KeyWord = new String[KEYWORD_MAX];
  String* KeyWordValue = new String[KEYWORD_MAX];
  bool keyWordEnable = false;

  SoftwareSerial *SerialAT;
  //std::unique_ptr<SoftwareSerial> serialInstance;

  bool timeEnable = false;
  String response; // read = read char by char
  String location;

  void handleResponse();
  void moduleSetup();
  void initiateRegistration();
  void networkSetup();
  void internetSetup();
  void UDPSocket(String IP, String port, char *data);
  void COAPDatagram(String IP, String port, char *data);

  char nibble2c(char c);
  char hex2c(char c1, char c2);
  String hex2str(char *data);


  public:
  NBIoT(const int RXPin = 14, const int TXPin = 2);
  ~NBIoT();

  bool begin();
  void write(char c);
  char read();

  void UDPConnect(String IP, String port, char *data);
  void COAPConnect(String IP, String port, char *data);

  String* getKeyToParse();
  void setKeyToParse(String givenKey[]);

};
