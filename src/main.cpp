#include <Arduino.h>
#include <SoftwareSerial.h>

//#include <string>


/*#define TX , RX on the esp8266,
  where Rx is IO13 used for Buzzer on Rysta
  and Tx is IO2 used for UART1tx */
#define RX 14
#define TX 2
SoftwareSerial SerialAT(RX, TX); // RX, TX
void res_sim800();
void initiate_registration();
void bearer_setup();
void internet_setup();
void location_data();
void http_req(int i, String web, String post_str);
void TCP_setup(int i, String URL, String path, String post, String Port_num);
void get_location();
void get_weather();
void post_rysta();

String response;
String devID = "KT34P22";

String time;
String location;
int time_enable;

String city;
int location_enable;

String temp;
String weather;
int weather_enable;

void setup() {
  Serial.begin(4800);
  while (!Serial) {
    ;
  }
  Serial.println("\n[ LOG ] Software serial test started");
  SerialAT.begin(4800);
  delay(1000);
  Serial.println("\n[ LOG ] Initializing modem...");

  initiate_registration();
  bearer_setup();
  internet_setup();
  location_data();

  String web_URL = "device.rysta-api.com";
  String filename = "";
  String path = "/v0/devices/" + devID + "/ping";
  String post_str = "time = "+ time +"; location = "+ location;

  get_location();
  if(city.length() > 1){
    get_weather();
  }
  if(temp.length() > 1){
    post_rysta();
  }






  /*
  web_URL = "ssh2.rysta-api.com";
  filename = "time" + time;
  Serial.println(filename);
  path = "/post_test?devID=" + devID + "&fileNAME=" + filename;
  post_str = "{\"myDevice\":\"teststse\",\"T\":\"323\",\"H\":\"12.23\"}";

  TCP_setup(2,web_URL,path,post_str,"8888");
  */







}

void loop() {
  Serial.println("\n[ LOG ] Loop started...");
  delay(2000);

}

void res_sim800(){
  response = "";
  int time_out = 0;
    while (time_out < 1001){

      while (SerialAT.available() > 0) {	//wait for data at software serial
        char c = SerialAT.read();
        response = response + c;
        Serial.write(c);	//Send data recived from software serial to hardware serial
      }

      if (time_out > 21) // stay at least 1 second or it will be too fast
      {
        if(time_enable == 1){
        time = response.substring(53,57) + response.substring(58,60) + response.substring(61,63) + response.substring(64,66) + response.substring(67,69);

        Serial.println(time);

        location = response.substring(43,52) +","+ response.substring(33,42);
        Serial.println(location);
        time_enable =0;
      }
        break;  }
      delay(51);
      time_out ++;
    }
    //Serial.println(response);
    if(time_enable == 1 && response.indexOf(",2017/") > 1 ){

      time = response.substring(53,57) + response.substring(58,60) + response.substring(61,63) + response.substring(64,66) + response.substring(67,69);
      Serial.println(time);
      location = response.substring(43,52) +","+ response.substring(33,42);
      Serial.println(location);
      time_enable = 0;
    }
    if(location_enable == 1 && response.indexOf("\"types\" : [ \"locality\", \"political\" ]") > 1){
      int ending_city = response.indexOf("\"types\" : [ \"locality\", \"political\" ]")-6;
      city = response.substring(ending_city-40,ending_city); //narrow the range of searching
      int beginning_city = city.indexOf("short_name")+15;
      city = city.substring(beginning_city);
      ending_city = city.indexOf('\"');
      city = city.substring(0, ending_city);

      Serial.println("\n[ LOG ] Name of the city is ...");
      Serial.println(city);
      location_enable = 0;
    }
    if( weather_enable == 1 && response.indexOf("temp") > 1){
      int beginning_weather = response.indexOf("\"description\"") + 15;
      weather = response.substring(beginning_weather);
      int endding_weather = weather.indexOf('\"');
      weather = weather.substring(0,endding_weather);

      int beginning_temp = response.indexOf("\"temp\"") + 7;
      temp = response.substring(beginning_temp);
      int endding_temp = temp.indexOf('\"')-1;
      temp = temp.substring(0,endding_temp);

      Serial.println("\n[ LOG ] Weather of the city is ...");
      Serial.println(weather);
      Serial.println("\n[ LOG ] Temperature of the city is ...");
      Serial.println(temp);

      weather_enable = 0;

    }

}

void initiate_registration(){
  Serial.println("\n[ LOG ] UNRegistering...");
  SerialAT.print(F("AT+CREG=0\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Registering with location information...");
  SerialAT.print(F("AT+CREG=2\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Checking registration status...");
  SerialAT.print (F("AT+CREG?\r\n"));
  res_sim800();
}

void bearer_setup(){
  Serial.println("\n[ LOG ] Configure bearer profile 1...");
  SerialAT.print(F("AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Configure bearer profile 1...");
  SerialAT.print(F("AT+SAPBR=3,1,\"APN\",\"web.vodafone.de\"\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Activate PDP... ");
  SerialAT.print(F("AT+CGACT=1,1\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Set up bearer 1 to open.... ");
  SerialAT.print(F("AT+SAPBR=1,1\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Check whether bearer 1 is open.... ");
  SerialAT.print(F("AT+SAPBR=2,1\r\n"));
  res_sim800();
}

void internet_setup(){
  Serial.println("\n[ LOG ] defines a PDP context... ");
  SerialAT.print(F("AT+CGDCONT=1,\"IP\",\"web.vodafone.de\"\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Check the PDP context...");
  SerialAT.print(F("AT+CGDCONT?\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] APN setting up ");
  SerialAT.print(F("AT+CSTT=\"web.vodafone.de\",\"\",\"\"\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] APN setting up check...");
  SerialAT.print(F("AT+CSTT?\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Start wireless connection with the GPRS... ");
  SerialAT.print(F("AT+CIICR\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Gets the IP address... ");
  SerialAT.print(F("AT+CIFSR\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Gets the signal strength... ");
  SerialAT.print(F("AT+CSQ\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Attach or Detach from GPRS Service... ");
  SerialAT.print(F("AT+CGATT=1\r\n"));
  res_sim800();
  Serial.println("[ LOG ] Check if it is attached or detached from GPRS Service... ");
  SerialAT.print(F("AT+CGATT?\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Enable error code... ");
  SerialAT.print(F("AT+CMEE=2\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Terminate HTTP server... ");
  SerialAT.print(F("AT+HTTPTERM\r\n"));
  res_sim800();
  Serial.println("\n[ LOG ] Disactivate PDP... ");
  SerialAT.print(F("AT+CGACT=0,1\r\n"));
  res_sim800();
}

void location_data(){

  time_enable = 1;
  Serial.println("[ LOG ] Test location and the time... ");
  SerialAT.print(F("AT+CIPGSMLOC=1,1\r\n"));

  int j = 0;
  while (j<20){
    res_sim800();
    delay(200);
    j++;
    if (location.length()>1){
      break;
    }
  }
}

void http_req(int i, String web, String post_str){

  Serial.println("\n[ LOG ] Initiate HTTP service... ");
  SerialAT.print(F("AT+HTTPINIT\r\n"));
  res_sim800();

  Serial.println("\n[ LOG ] Set the HTTP bearer number... ");
  SerialAT.print(F("AT+HTTPPARA=\"CID\",1\r\n"));
  res_sim800();

  String str = "AT+HTTPPARA=\"URL\",\"" + web + "\"\r\n";
  Serial.println("\n[ LOG ] Set the HTTP URL... ");
  SerialAT.print(str);
  res_sim800();
  /*
  Serial.println("\n[ LOG ] Test the HTTP parameter setup... ");
  SerialAT.print("AT+HTTPPARA=?");
  res_sim800();

  Serial.println("\n[ LOG ] Read the HTTP parameter setup... ");
  SerialAT.print("AT+HTTPPARA?");
  res_sim800();


  str = "AT+HTTPPARA=\"USERDATA\",\"Devid: " + devID + ";authkey: " + authKey + "\",\";\"";
  Serial.println("\n[ LOG ] Set the HTTP header \"devID & authKey\"... ");
  SerialAT.print(str);
  res_sim800();
  */


  switch (i){
    case 1: //GET
      Serial.println("\n[ LOG ] GET request is initiated... ");

      Serial.println("\n[ LOG ] Start the session... ");
      SerialAT.print(F("AT+HTTPACTION=0\r\n"));
      res_sim800();
      delay(1000);
      Serial.println("\n[ LOG ] To read the data of the HTTP server... ");
      SerialAT.print(F("AT+HTTPREAD\r\n"));
      res_sim800();
      break;

    case 2: //POST
      Serial.println("\n[ LOG ] POST request is initiated... ");


      Serial.println("\n[ LOG ] Set the HTTP header \"application/json\"... ");
      SerialAT.print("AT+HTTPPARA=\"CONTENT\",\"application/json\"");
      res_sim800();
      delay(2000);
      res_sim800();
      delay(2000);
      res_sim800();

      int post_length = post_str.length();
      String cmd = "AT+HTTPDATA=" + String(post_length) +",10000\r\n";

      Serial.println("\n[ LOG ] Start the HTTP session... ");
      SerialAT.print(cmd);
      res_sim800();
      Serial.println("\n[ LOG ] POSTing... ");
      SerialAT.print(post_str);
      res_sim800();
      Serial.println("\n[ LOG ] Start the HTTP session... ");
      SerialAT.print(F("AT+HTTPACTION=1\r\n"));
      res_sim800();
      delay(1000);
      Serial.println("\n[ LOG ] To read the data of the HTTP server... ");
      SerialAT.print(F("AT+HTTPREAD\r\n"));
      res_sim800();
      delay(1000);
      res_sim800();
      delay(1000);
      res_sim800();
      break;
  }
  Serial.println("\n[ LOG ] Terminate HTTP server... ");
  SerialAT.print(F("AT+HTTPTERM\r\n"));
  res_sim800();
}

void TCP_setup(int i, String URL, String path, String post, String Port_num){

  Serial.println("\n[ LOG ] Select Data Transmitting Mode... ");
  SerialAT.print(F("AT+CIPQSEND=1\r\n"));
  res_sim800();

  String str = "AT+CIPSTART=\"TCP\",\"" + URL + "\"," + Port_num + "\r\n";
  Serial.println("\n[ LOG ] set up TCP connection... ");
  SerialAT.print(str);
  res_sim800();
  delay(100);
  res_sim800();

  switch(i){
    case 1: str = "GET " + path +" HTTP/1.1\r\n";
            str = str + "Host: " + URL + "\r\n";
            //str = str + "Connection: keep-alive\r\n";
            str = str + "Content-type: application/json\r\n";
            //str = str + "Accept: */*\r\n";
            str = str + "Cache-Control: no-cache\r\n";
            str = str + "\r\n\r\n";
            break;
    case 2: str = "POST " + path +" HTTP/1.1\r\n";
            str = str + "Host: " + URL + "\r\n";
            str = str + "Content-Type: application/json\r\n";
            str = str + "Connection: keep-alive\r\n  \r\n";
            str = str + post +"\r\n\r\n";
            break;
  }


  int post_length = str.length();
  String cmd = "AT+CIPSEND=" + String(post_length) + "\r\n";
  int j = 0;


  switch(i){
    case 1:
    Serial.println("\n[ LOG ] GET... ");
    Serial.println("\n[ LOG ] Send data through TCP connection... ");
    SerialAT.print(cmd);
    res_sim800();
    delay(100);
    SerialAT.print(str);
    res_sim800();
    while( j < 60 && (location_enable != 0 || weather_enable != 0)){
      res_sim800();
      delay(500);
      j++;
    }
    SerialAT.print("\r\n");
    SerialAT.print("\r\n");
    Serial.println("\n[ LOG ] Check IP connection status... ");
    SerialAT.print(F("AT+CIPSTATUS\r\n"));
    res_sim800();
    break;

    case 2:
    Serial.println("\n[ LOG ] POST... ");
    Serial.println("\n[ LOG ] Send data through TCP connection... ");
    SerialAT.print(cmd);
    res_sim800();
    delay(100);
    SerialAT.print(str);
    res_sim800();
    while( j < 60 ){
      res_sim800();
      delay(500);
      j++;
    }
    Serial.println("\n[ LOG ] Check IP connection status... ");
    SerialAT.print(F("AT+CIPSTATUS\r\n"));
    res_sim800();
    break;
  }

  Serial.println("\n[ LOG ] Check IP connection status... ");
  SerialAT.print(F("AT+CIPSTATUS\r\n"));
  res_sim800();

  Serial.println("\n[ LOG ] Close IP connection... ");
  SerialAT.print(F("AT+CIPCLOSE\r\n"));
  res_sim800();

}

void get_location(){
  location_enable = 1;
  String web_URL = "maps.googleapis.com";
  String path = "/maps/api/geocode/json?latlng="+ location +"&amp;result_type=administrative_area_level_1&amp;key=AIzaSyDnMUmBQYDygc-6-L74HE2dVkofI_JT-IA";
  String post_str = "";
  TCP_setup(1,web_URL,path,post_str,"80");
}

void get_weather(){

  String web_URL = "api.openweathermap.org";
  String path = "/data/2.5/weather?q="+ city +"&appid=96b8c7454acfb29cbbc20835282972ee";
  String post_str = "";
  weather_enable = 1;
  TCP_setup(1,web_URL,path,post_str,"80");

}

void post_rysta(){

  String filename = "time" + time;
  String web_URL = "ssh2.rysta-api.com";
  String path = "/post_test?devID=" + devID + "&fileNAME=" + filename;
  String post_str = "{\"location\":\""+ city;
  post_str = post_str + "\",\"temperature\":\"" + temp + "\",\"Weather\":\"" + weather + "\"}";
  TCP_setup(2,web_URL,path,post_str,"80");

}
